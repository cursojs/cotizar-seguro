function Seguro(marca, anio, tipo) {
    this.marca = marca;
    this.anio = anio;
    this.tipo = tipo;
}

Seguro.prototype.cotizarSeguro = function() {
   let cantidad;
   const base = 2000;

   switch (this.marca) {
      case "1":
        cantidad = base * 1.15;
        break;
   
      case "2":
        cantidad = base * 1.05;
        break;

      case "3":
        cantidad = base * 1.35;
        break;
   }

   const diferencia = new Date().getFullYear() - this.anio;
   cantidad -= ((diferencia * 3) * cantidad) / 100;

   cantidad *= this.tipo === 'basico' ? 1.30 : 1.50;
   return cantidad;
}

function Interfaz() {}
Interfaz.prototype.mostrarMensaje = function(mensaje, tipo) {
    const div = document.createElement('div');
    const error = tipo === 'error' ? 'error' : 'correcto';

    div.classList.add('mensaje', error);
    div.innerHTML = `${mensaje}`;
    formulario.insertBefore(div, document.querySelector('.form-group'));

    setTimeout(() => {
        document.querySelector('.mensaje').remove();
    }, 3000);
}

Interfaz.prototype.mostrarResultado = function(seguro, total) {
    const resultado = document.querySelector('#resultado');
    let marca;
    switch (seguro.marca) {
      case '1':
        marca = 'Americano'
        break;

      case '2':
        marca = 'Asiatica'
        break;

      case '3':
        marca = 'Europeo'
        break;
    }
    
    const div = document.createElement('div');
    div.innerHTML = `
        <p class="header">Tu Resumen:</p>
        <p>Marca: ${marca}</p>
        <p>Año: ${seguro.anio}</p>
        <p>Tipo: ${seguro.tipo}</p>
        <p>Total: ${total}</p>
    `;

    const spinner = document.querySelector('#cargando img');
    spinner.style.display = 'block';

    setTimeout(() => {
        spinner.remove();
        resultado.appendChild(div);
    }, 2000);

}

const formulario = document.querySelector('#cotizar-seguro');

formulario.addEventListener('submit', function(e) {
    e.preventDefault();

    const marca = document.querySelector('#marca');
    const selectMarca = marca.options[marca.selectedIndex].value;


    const anio = document.querySelector('#anio');
    const selectAnio = anio.options[anio.selectedIndex].value;

    const tipo = document.querySelector('input[name="tipo"]:checked').value;

    const interfaz = new Interfaz();
    if (selectMarca === '' || selectAnio === '' || tipo === '') {
        interfaz.mostrarMensaje('Falta datos, revisar el formulario para volver a cotizar', 'error');
    } else {
        const resultado = document.querySelector('#resultado div');
        if(resultado !== null) {
            resultado.remove();
        }

        const seguro = new Seguro(selectMarca, selectAnio, tipo);
        const cantidad = seguro.cotizarSeguro();
        interfaz.mostrarResultado(seguro, cantidad);

        interfaz.mostrarMensaje('Cotizacion realizada', 'correcto');
    }
})

const max = new Date().getFullYear();
const min = max - 20;
const selectYear = document.querySelector('#anio');

for (let i = max; i > min; i--) {
    let option = document.createElement('option');
    option.value = i;
    option.innerHTML = i;
    selectYear.appendChild(option);
}